<?php

namespace Drupal\Tests\minimum_password_length\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * The MinimumPasswordLengthTest class.
 *
 * @group minimum_length_password
 */
class MinimumPasswordLengthTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'minimum_length_password',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->config('user.settings')->set('verify_mail', FALSE)->save();
  }

  /**
   * Test the password minimum length.
   */
  public function testMinimumLength() {
    $this->drupalPostForm('user/register', [
      'mail' => 'foo@example.com',
      'name' => 'test user',
      'pass[pass1]' => 'foo',
      'pass[pass2]' => 'foo',
    ], 'Create new account');
    $this->assertSession()->elementContains('css', '.messages--error', 'Password must be at least 12 characters long.');

    $this->submitForm([
      'pass[pass1]' => 'foofoofoofoofoo',
      'pass[pass2]' => 'foofoofoofoofoo',
    ], 'Create new account');
    $this->assertSession()->elementContains('css', '.messages--status', 'Registration successful.');
  }

}
