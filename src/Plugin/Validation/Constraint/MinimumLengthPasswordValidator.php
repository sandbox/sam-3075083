<?php

namespace Drupal\minimum_length_password\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the password length constraint.
 */
class MinimumLengthPasswordValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($password, Constraint $constraint) {
    $password_length = mb_strlen($password->value);
    if ($password_length < MinimumLengthPassword::MINIMUM_LENGTH) {
      $this->context->addViolation($constraint->message, array('@minimum_password_length' => MinimumLengthPassword::MINIMUM_LENGTH));
    }
  }

}
