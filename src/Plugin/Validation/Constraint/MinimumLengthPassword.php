<?php

namespace Drupal\minimum_length_password\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if the user's password meets minimum password length requirements.
 *
 * @Constraint(
 *   id = "MinimumLengthPassword",
 *   label = @Translation("Minimum password length", context = "Validation")
 * )
 */
class MinimumLengthPassword extends Constraint {

  /**
   * The minimum password length to enforce.
   */
  const MINIMUM_LENGTH = 12;

  /**
   * Violation message.
   *
   * @var string
   */
  public $message = 'Password must be at least @minimum_password_length characters long.';

}
